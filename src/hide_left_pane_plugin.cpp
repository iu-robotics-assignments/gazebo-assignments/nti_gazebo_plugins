
#include <iostream>

#include <gazebo/common/Plugin.hh>
#include <gazebo/gui/GuiPlugin.hh>
#include <gazebo/gui/MainWindow.hh>
#include <gazebo/gui/GuiIface.hh>


class HideLeftPane : public gazebo::GUIPlugin
{
//    Q_OBJECT
public:
    HideLeftPane() : gazebo::GUIPlugin() {
	auto mainLayout = new QHBoxLayout;
	mainLayout->setContentsMargins(0, 0, 0, 0);
	this->setLayout(mainLayout);
    }

    void Load(sdf::ElementPtr) {
        auto mainWindow = gazebo::gui::get_main_window();
	std::cout << "I've got the main window!" << std::endl;
	mainWindow->SetLeftPaneVisibility(false);
    }
};

GZ_REGISTER_GUI_PLUGIN(HideLeftPane)


