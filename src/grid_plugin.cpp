#include <functional>
#include <gazebo/gui/GuiIface.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/gazebo.hh>

namespace gazebo
{
  class GridPlugin : public SystemPlugin
  {
  public:
    void Load(int /*_argc*/, char ** /*_argv*/)
    {
      this->connections_.push_back(event::Events::ConnectRender(boost::bind(&GridPlugin::Update, this)));
    }

  private:
    void Update()
    {
      rendering::ScenePtr scene = rendering::get_scene();

      if (!scene || !scene->Initialized())
      {
        return;
      }

      auto grid = scene->GetGrid(0);
      if (!grid)
        return;
        
      // value in meters
      grid->SetCellLength(0.6);
    }

  private:
    std::vector<event::ConnectionPtr> connections_;
  };

  GZ_REGISTER_SYSTEM_PLUGIN(GridPlugin)
}
